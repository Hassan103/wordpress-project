

$(document).ready(function(){
    
    /* BLOGS THEME */
    $('.slider-blog-items').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        prevArrow:'<span class="a-left control-c prev slick-prev"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 490.4 490.4" width="30" height="30"><path fill="#fa9c02" d="M245.2 490.4c135.2 0 245.2-110 245.2-245.2S380.4 0 245.2 0 0 110 0 245.2s110 245.2 245.2 245.2zm0-465.9c121.7 0 220.7 99 220.7 220.7s-99 220.7-220.7 220.7-220.7-99-220.7-220.7 99-220.7 220.7-220.7z"></path><path fill="#ffffff" d="M138.7 257.5h183.4l-48 48c-4.8 4.8-4.8 12.5 0 17.3 2.4 2.4 5.5 3.6 8.7 3.6s6.3-1.2 8.7-3.6l68.9-68.9c4.8-4.8 4.8-12.5 0-17.3l-68.9-68.9c-4.8-4.8-12.5-4.8-17.3 0s-4.8 12.5 0 17.3l48 48H138.7c-6.8 0-12.3 5.5-12.3 12.3 0 6.8 5.5 12.2 12.3 12.2z"></path></svg></span>',
        nextArrow:'<span class="a-right control-c next slick-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 490.4 490.4" width="30" height="30"><path fill="#fa9c02" d="M245.2 490.4c135.2 0 245.2-110 245.2-245.2S380.4 0 245.2 0 0 110 0 245.2s110 245.2 245.2 245.2zm0-465.9c121.7 0 220.7 99 220.7 220.7s-99 220.7-220.7 220.7-220.7-99-220.7-220.7 99-220.7 220.7-220.7z"></path><path fill="#ffffff" d="M138.7 257.5h183.4l-48 48c-4.8 4.8-4.8 12.5 0 17.3 2.4 2.4 5.5 3.6 8.7 3.6s6.3-1.2 8.7-3.6l68.9-68.9c4.8-4.8 4.8-12.5 0-17.3l-68.9-68.9c-4.8-4.8-12.5-4.8-17.3 0s-4.8 12.5 0 17.3l48 48H138.7c-6.8 0-12.3 5.5-12.3 12.3 0 6.8 5.5 12.2 12.3 12.2z"></path></svg></span>',
          responsive: [
            {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1, 
            }
            },
            {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
    });
    /* END BLOGS THEME */

    /* BLOGS THEME */
    $('.feature-blog').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 2,
        centerMode: true,
        CenterPadding: '100px',
        dots:true,
        nav:false,
        arrows:false,
        responsive: [
            {
            breakpoint: 1300,
            settings: {
                arrows: false,
                slidesToShow: 3,
                slidesToShow: 3,
                centerMode: true, 
                CenterPadding: '0px',
            }
            },
            {
            breakpoint: 1200,
            settings: {
               
                slidesToShow: 2,
                slidesToShow: 2,
                centerMode: true, 
                CenterPadding: '0px',
            }
            },
            {
            breakpoint: 991,
            settings: { 
                slidesToShow: 2,
                slidesToShow: 2,
                centerMode: true,
                CenterPadding: '0px',
             
            }
            },
            {
            breakpoint: 750,
            settings: { 
               arrows: false,
                slidesToShow: 1,
                slidesToShow: 1,
                centerMode: true, 
                CenterPadding: '0px',
             
            }
            },
             
    ]

    });

    /* END BLOGS THEME */


 

})
window.addEventListener('load', function () {
   $('p:empty').remove();
}, false);

