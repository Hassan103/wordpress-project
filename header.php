<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wisdom Bob</title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <?php wp_head(); ?> 
</head>

    <?php if ( is_home() ) : 
        $page_class = array('home-page');
        else :
        $page_class = array('post-details-page');
        endif;
        echo is_front_page();
    ?>


<body <?php body_class( $page_class ) ?>>
    
<!-- Header Section -->
    <header>
        <div class="header-nav">
             <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 ">
                        <nav class="navbar navbar-expand-lg navbar-light main-navigation p-0">
                            <div class="col-md-12 pr-0">
                                <div class="row">
                                    <div class="col-lg-6"> 
                                        <?php   if ( function_exists( the_custom_logo(' sad')) ) { 
                                                the_custom_logo();}
                                        ?>
                                    </div> 
                                    <div class="col-lg-6 pl-0 white-bg-after">
                                        <button class="menu navbar-toggler" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation" onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))" aria-label="Main Menu">
                                            <svg width="40" height="40" viewBox="0 0 100 100">
                                                <path class="line line1" d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
                                                <path class="line line2" d="M 20,50 H 80" />
                                                <path class="line line3" d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
                                            </svg>
                                        </button>
                                        <?php 
                                             wp_nav_menu( array(
                                                 'theme_location'  => 'primary-menu',
                                                 'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                                                 'container'       => 'div',
                                                 'container_class' => 'collapse navbar-collapse',
                                                 'container_id'    => 'navbarNavDropdown',
                                                 'menu_class'      => 'navbar-nav mr-auto',
                                                 'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                                 'walker'          => new WP_Bootstrap_Navwalker(),
                                             ) );
                                        ?>
                                        <!-- <div class="collapse navbar-collapse" id="navbarNavDropdown">
                                            <ul class="navbar-nav w-100 align-items-center">
                                                <li class="nav-item">                                                                                        
                                                    <a class="nav-link active" href="#" class="active">Home <span class="sr-only">(current)</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Blogs</a>
                                                </li>
                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Categories
                                                    </a>
                                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                                        <a class="dropdown-item" href="#">Action</a>
                                                        <a class="dropdown-item" href="#">Another action</a>
                                                        <a class="dropdown-item" href="#">Something else here</a>
                                                    </div>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">Contact Us</a>
                                                </li>
                                                <li class="nav-item ml-lg-auto">
                                                    
                                                </li>
                                            </ul>
                                        </div> -->
                                        <div class="headernav-social-media">
                                            <div class="social-icons">
                                                <a href="javascript:void(0)">
                                                    <span class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="20" height="18"><path d="M12 27V15H8v-4h4V8.852C12 4.785 13.981 3 17.361 3c1.619 0 2.475.12 2.88.175V7h-2.305C16.501 7 16 7.757 16 9.291V11h4.205l-.571 4H16v12h-4z"/></svg></span>
                                                </a>
                                                <a href="javascript:void(0)" class="ml-2">
                                                    <span class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="20" height="15"><path d="M50.063 10.438a20.57 20.57 0 01-5.91 1.62 10.309 10.309 0 004.523-5.687 20.648 20.648 0 01-6.531 2.492 10.258 10.258 0 00-7.504-3.246c-5.68 0-10.286 4.602-10.286 10.281 0 .805.094 1.59.27 2.344-8.547-.43-16.121-4.523-21.195-10.746a10.243 10.243 0 00-1.39 5.172c0 3.566 1.812 6.715 4.573 8.562a10.274 10.274 0 01-4.66-1.289v.13c0 4.984 3.547 9.136 8.246 10.085a10.29 10.29 0 01-4.644.172c1.312 4.082 5.11 7.063 9.605 7.145A20.613 20.613 0 012.39 41.87c-.831 0-1.648-.047-2.449-.144a29.053 29.053 0 0015.762 4.62c18.914 0 29.258-15.667 29.258-29.253 0-.446-.012-.895-.027-1.332a20.904 20.904 0 005.129-5.325z"/></svg></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </nav>
                    </div>
                </div>
            </div>
        </div>

    <!-- Main Slider Section -->



    <?php if( is_home() ) :  ?>

    <div class="s-bg"></div>
    <div class="main-slider-section">
        <div class="s-bg-icons"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="slider-img-section">
                        <img src="<?php echo get_template_directory_uri() . '/images/slider-section-img.png' ?>" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-8">
                        <?php 
                    // query
                    $data = [
                        'post_type'			=> 'post',
                        'posts_per_page'	=> 1,
                        'meta_key'			=> 'is_feature',
                        'orderby'			=> 'meta_value',
                        'order'				=> 'ASC'
                    ];
                    $features_blogs = new WP_Query($data);
                ?> 
                    <div class="slider-section-text">
                        <?php if( $features_blogs->have_posts() ) :  
                            while( $features_blogs->have_posts() ) : $features_blogs->the_post() ?>        
                            <h1><?=  wp_trim_words( get_the_title(), 8, '...' ); ?></h1>
                            <p><?php echo wp_trim_words( get_the_content(), 35, '...' );  ?></p>
                            <div class="slider-btn">
                                <a href="<?php the_permalink(); ?>" class="theme-btn __primary-btn __lg-btn">Read More</a>
                            </div>
                    </div>
                         <?php endwhile; ?>
                    <?php endif; ?>
                    <div class="slider-blog">
                        <h4>Recent Posted</h4>
                        <div class="slider-blog-items">
                        <?php 
                        // Define our WP Query Parameters
                        $the_query = new WP_Query( 'posts_per_page=3' ); ?> 
                        <!-- // Start our WP Query  -->
                        <!-- Display the Post Title with Hyperlink   -->
                        <?php    while ($the_query -> have_posts()) : $the_query -> the_post();     ?>
                            <div class="item"> 
                                <div class="cat-widget mb-4">
                                    <div class="cat-name">
                                        <?php the_category(' '); ?>
                                    </div>
                                    <h5> <?php the_title(); ?> </h5>
                                    <p><?php echo wp_trim_words( get_the_content(), 25, '...' ); ?></p> 
                                    <div class="wd-details d-flex justify-content-between">
                                        <span class="date">
                                            <i class="iocn"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#cccccc" width="15" height="15"><path d="M12 0C5.371 0 0 5.371 0 12s5.371 12 12 12 12-5.371 12-12S18.629 0 12 0zm0 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2zm-1.063 1.875l-.437 8.188.094.874 6.156 5.438.969-1-5.094-5.406-.438-8.094z"></path></svg></i>
                                            <?php the_time('F j, Y') ?>
                                                </span>
                                        <div class="bp-btn">
                                            <a href="<?php the_permalink(); ?>" class="m-0">Read More <span class="ml-0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 490.4 490.4" width="22" height="22">
                                            <path d="M245.2 490.4c135.2 0 245.2-110 245.2-245.2S380.4 0 245.2 0 0 110 0 245.2s110 245.2 245.2 245.2zm0-465.9c121.7 0 220.7 99 220.7 220.7s-99 220.7-220.7 220.7-220.7-99-220.7-220.7 99-220.7 220.7-220.7z" class="path1"></path>
                                            <path class="path2" d="M138.7 257.5h183.4l-48 48c-4.8 4.8-4.8 12.5 0 17.3 2.4 2.4 5.5 3.6 8.7 3.6s6.3-1.2 8.7-3.6l68.9-68.9c4.8-4.8 4.8-12.5 0-17.3l-68.9-68.9c-4.8-4.8-12.5-4.8-17.3 0s-4.8 12.5 0 17.3l48 48H138.7c-6.8 0-12.3 5.5-12.3 12.3 0 6.8 5.5 12.2 12.3 12.2z"></path></svg></span></a>
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                            <?php 
                            // Repeat the process and reset once it hits the limit
                            endwhile;
                            wp_reset_postdata();
                            ?> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
else : ?>
<?php  endif; ?>




    <!-- End Main Slider Section -->
    </header>
<!-- Header Section -->