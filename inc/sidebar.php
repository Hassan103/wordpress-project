<?php  $postid = get_the_ID(); ?>
  <!-- Widget Area -->
                        <div class="side-widgetbar">
                            <?php if($postid != 24) : ?>
                            <!-- New Letter -->
                            <?php echo do_shortcode( '[contact-form-7 id="116" title="Newsletter"]'); ?>
                            <!-- End New Letter -->
                            <?php endif; ?>
                            <!-- Topics -->
                            <div class="side-widget __topics-widget br-20">
                                <h4>Topics</h4>
                                <div class="topic-listing">
                                    <ul class="default-list">
                                        <?php
                                            $args = array(
                                                'orderby' => 'slug',
                                                'parent' => 0
                                            );
                                            $categories = get_categories( $args );
                                            foreach( $categories as $category ):
                                        ?>
                                        <li>
                                            <span class="subject">
                                                <a href="<?= get_category_link( $category->term_id ) ?>">
                                                    <?= $category->name ?>
                                                </a>
                                            </span>
                                            <span class="dash"></span>
                                            <span class="no-topics"><?= $category->category_count ?></span>
                                        </li>
                                        <?php endforeach ?>
                                    </ul>
                                </div> 
                            </div>
                            <!-- End Topics -->
                            <!-- Social Media -->
                            <div class="side-widget __socialmedia-widget br-20">
                                <h4>Let’s Get Social</h4>
                                   <p>Make sure to subscribe to our
                                    newsletter and be the first to know
                                    the news.</p>
                                <div class="w-social-media">
                                    <div class="social-icons">
                                        <a href="javascript:void(0)">
                                            <span class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" width="20" height="18"><path d="M12 27V15H8v-4h4V8.852C12 4.785 13.981 3 17.361 3c1.619 0 2.475.12 2.88.175V7h-2.305C16.501 7 16 7.757 16 9.291V11h4.205l-.571 4H16v12h-4z"/></svg></span>
                                        </a>
                                        <a href="javascript:void(0)" class="ml-2">
                                            <span class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" width="20" height="15"><path d="M50.063 10.438a20.57 20.57 0 01-5.91 1.62 10.309 10.309 0 004.523-5.687 20.648 20.648 0 01-6.531 2.492 10.258 10.258 0 00-7.504-3.246c-5.68 0-10.286 4.602-10.286 10.281 0 .805.094 1.59.27 2.344-8.547-.43-16.121-4.523-21.195-10.746a10.243 10.243 0 00-1.39 5.172c0 3.566 1.812 6.715 4.573 8.562a10.274 10.274 0 01-4.66-1.289v.13c0 4.984 3.547 9.136 8.246 10.085a10.29 10.29 0 01-4.644.172c1.312 4.082 5.11 7.063 9.605 7.145A20.613 20.613 0 012.39 41.87c-.831 0-1.648-.047-2.449-.144a29.053 29.053 0 0015.762 4.62c18.914 0 29.258-15.667 29.258-29.253 0-.446-.012-.895-.027-1.332a20.904 20.904 0 005.129-5.325z"/></svg></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Social Media -->
                            <!-- Most Read Blogs -->
                            <!--  MOST VIEWED POST QUERY -->
                            <?php 
                                $data = [
                                    'posts_per_page' => 4,
                                    'meta_key' => 'wpb_post_views_count',
                                    'orderby' => 'meta_value_num',
                                    'order' => 'DESC',
                                ];
                                $most_read_blogs = new WP_Query( $data );
                            ?>
                            <!--  MOST VIEWED POST QUERY -->
            
                            <?php if($postid != 24) : ?>
                              <div class="most-read-blogs">
                                <h3>Most Read Blogs</h3>
                                <?php while( $most_read_blogs->have_posts() ) :  $most_read_blogs->the_post() ?>
                                    <div class="cat-widget mb-4">
                                        <div class="cat-name"><?php the_category(' '); ?></div>
                                        <h5> <?php echo wp_trim_words( get_the_title(), 4, '...' ); ?> </h5>
                                        <div class="wd-details d-flex justify-content-between">
                                            <span class="date">
                                                <i class="iocn"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#cccccc" width="15" height="15"><path d="M12 0C5.371 0 0 5.371 0 12s5.371 12 12 12 12-5.371 12-12S18.629 0 12 0zm0 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2zm-1.063 1.875l-.437 8.188.094.874 6.156 5.438.969-1-5.094-5.406-.438-8.094z"></path></svg></i>
                                                <?php the_time('F j, Y'); ?>
                                            </span>
                                            <div class="bp-btn">
                                                <a href="<?php the_permalink(); ?>">Read More <span class="ml-0"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 490.4 490.4" width="22" height="22">
                                                <path d="M245.2 490.4c135.2 0 245.2-110 245.2-245.2S380.4 0 245.2 0 0 110 0 245.2s110 245.2 245.2 245.2zm0-465.9c121.7 0 220.7 99 220.7 220.7s-99 220.7-220.7 220.7-220.7-99-220.7-220.7 99-220.7 220.7-220.7z" class="path1"></path>
                                                <path class="path2" d="M138.7 257.5h183.4l-48 48c-4.8 4.8-4.8 12.5 0 17.3 2.4 2.4 5.5 3.6 8.7 3.6s6.3-1.2 8.7-3.6l68.9-68.9c4.8-4.8 4.8-12.5 0-17.3l-68.9-68.9c-4.8-4.8-12.5-4.8-17.3 0s-4.8 12.5 0 17.3l48 48H138.7c-6.8 0-12.3 5.5-12.3 12.3 0 6.8 5.5 12.2 12.3 12.2z"></path></svg></span></a>
                                            </div>
                                        </div>
                                    </div>                             
                                <?php endwhile; ?>
                            </div>  
                            <!-- End Most Read Blogs -->
                            <?php endif; ?>

                        </div> 
                <!-- Widget Area -->