<?php 
    get_header();
    $current_cat = get_queried_object();
   
?> 

 



   
    
 <?php  if($current_cat->count == 0):  ?>
<div id="primary" class="site-content">
<div id="content" role="main">

<div class="inner-header-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-head">
                    <h2><?= $current_cat->name ?></h2>
                    <!-- <p><?// = $current_cat->description ?></p> -->
                </div>
            </div>
        </div>
    </div>  
</div> 
    <div class="blogpost-inner-details main-wrap-blogs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="alert alert-danger">No Category Found</h4>
                </div>
            </div>
        </div>
    </div>
<?php else : ?>
<?php if ( have_posts() ) : ?>

    <div class="inner-header-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner-head">
                        <h2><?= $current_cat->name ?></h2>
                        <!-- <p><?// = $current_cat->description ?></p> -->
                    </div>
                </div>
            </div>
        </div>  
    </div> 
<!-- BLOG POST DETAILS -->    
<div class="blogpost-inner-details main-wrap-blogs">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="inner-blog-details">
                    <?php while ( have_posts() ) : the_post(); ?>
                    <div class="blogpost-box">
                        <div class="bp-img">                                                
                            <figure>
                                <?php  
                                    if ( has_post_thumbnail() ) :  
                                        the_post_thumbnail();
                                            else : ?>
                                    <img src="<?php echo get_template_directory_uri() . '/images/placeholder_featured_image.svg' ?>" alt="" class="img-fluid">    
                                <?php endif; ?>
                            </figure>
                        </div>
                        <div class="bp-details">
                            <span class="catergory"><?php the_category(' ') ?></span>
                            <h2> <?php the_title(); ?>  </h2>
                            

                            <p> <?php the_content( );  ?> </p> 
                            <div class="d-flex align-items-center justify-content-between auther-details">
                                <p class="m-0 d-flex align-items-center"> <span class="icon d-inline-block"><svg xmlns="http://www.w3.org/2000/svg" fill="orange" viewBox="0 0 128 128" width="23" height="23"><path d="M56 21.9c-12.7 0-23 10.3-23 23 0 1.7 1.3 3 3 3h5.4c11.5 0 23.2 0 31.6-7.5v.1c.4-.4.8-.7 1.1-1.1 1.1-1.2 3-1.4 4.2-.3 1.3 1.1 1.4 3.1.2 4.4l-1.4 1.4c-1.3 1.3-.8 3.5.9 4.2 1 .4 2.1.6 3.2.7 1.5.2 2.7 1.4 2.7 3v6.8C84 70.1 76 79.2 65.4 80c-11.6.8-21.3-8.4-21.4-19.9 0-1.6-1.2-3-2.8-3.1-1.7-.1-3.2 1.3-3.2 3 0 14.3 11.7 26 26 26 13.9 0 25.3-10.9 26-24.7V43c0-7.2-5.4-13.1-12.4-13.9l-.4-.6c-2.9-4.1-7.7-6.6-12.7-6.6H56zM64 91c-16.4 0-31.6 8.9-39.7 23.1-.8 1.4-.3 3.3 1.1 4.1.5.3 1 .4 1.5.4 1 0 2.1-.5 2.6-1.5C36.6 104.7 49.8 97 64 97s27.4 7.7 34.5 20.1c.8 1.4 2.7 1.9 4.1 1.1 1.4-.8 1.9-2.7 1.1-4.1C95.6 99.9 80.4 91 64 91z"/></svg></span> Author: <span class="author"><?php the_author(); ?></span></p>
                                <span class="bp-date-time"> 
                                    <i>
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="orange" width="18" height="18"><path d="M12 0C5.371 0 0 5.371 0 12s5.371 12 12 12 12-5.371 12-12S18.629 0 12 0zm0 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2zm-1.063 1.875l-.437 8.188.094.874 6.156 5.438.969-1-5.094-5.406-.438-8.094z"></path></svg>
                                    </i> <?php the_time('F j, Y') ?>
                                </span>
                            </div>                                
                        </div>
                    </div>
                    <?php endwhile; ?>
                <?php endif; ?>
                </div>
                <nav class="pagination"><?php pagination_bar(); ?></nav>
            </div>
            <div class="col-md-4">
                <div class="inner-sidebar">
                    <!-- Sidebar -->
                        <?php require_once get_template_directory() . '/inc/sidebar.php'; ?>
                        <?php // get_sidebar(); ?>
                    <!-- End Sidebar -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif;  ?>


<!-- END BLOG POST DETAILS -->
<?php get_footer(); ?>