<?php 

/****************************
 Adding Bootstrap Navigation
****************************/
require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
/****************************
End Adding Bootstrap Navigation
****************************/


/****************************
 Adding CSS Files 
 ****************************/

function wisdombob_script_enqueue(){
    wp_enqueue_style('customstyle', get_template_directory_uri(). '/css/style.css', array(), '1.0.0', 'all');
    wp_enqueue_style('bootstrapstyle', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css', array(), '1.0.0', 'all');
    wp_enqueue_style('slickcss', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), '1.0.0', 'all');
    wp_enqueue_script('jqyery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js',  array(), '', true );
    wp_enqueue_script('popperjs', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js',  array(), '', true );
    wp_enqueue_script('bootstrapjs', 'https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js',  array(), '', true );
    wp_enqueue_script('slickjs', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js',  array(), '', true );
    wp_enqueue_script('customjs', get_template_directory_uri(). '/js/custom.js', array(), '1.0.0', true);
}


add_action('wp_enqueue_scripts', 'wisdombob_script_enqueue');

function wisdombob_theme_support(){
    add_theme_support( 'menus' );
    register_nav_menu('primary-menu', 'Primary Header Navigation');       
}

add_action('init', 'wisdombob_theme_support');
add_theme_support('post-thumbnails');
add_theme_support('custom-widget');


/****************************
    MOST VIEWED POST
 ****************************/
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');


/****************************
END MOST VIEWED POST
 ****************************/

/****************************
WIDGET AREA 
 ****************************/

// FOOTER 1 or QUCIK-LINKS
function quick_links_widget_init() {
    $data = [
        'name'          => 'QUICK LINKS',
        'id'            => 'quicklink_widget',
        'description'   => 'Add Footer Qucik Links Here...',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h5>',
        'after_title'   => '</h5>',
    ];
    register_sidebar($data);
}
add_action( 'widgets_init', 'quick_links_widget_init' );
// End FOOTER 1 or QUCIK-LINKS

// FOOTER 2 or CATEGORIES
function category_widget_init() {
    $data = [
        'name'          => 'CATEGORIES',
        'id'            => 'category_widget',
        'description'   => 'Add Footer Category Links Here...',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h5>',
        'after_title'   => '</h5>',
    ];
    register_sidebar($data);
}
add_action( 'widgets_init', 'category_widget_init' );
// END FOOTER 2 or CATEGORIES

// FOOTER 3 or Contact
function contact_widget_init() {
    $data = [
        'name'          => 'CONTACT',
        'id'            => 'contact_widget',
        'description'   => 'Add Footer contact HTML Here...',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h5>',
        'after_title'   => '</h5>',
    ];
    register_sidebar($data);
}
add_action( 'widgets_init', 'contact_widget_init' );
// END FOOTER 3 or Contact


// REMOVE EXTRA P TAGS
remove_filter('the_content', 'wpautop');


/****************************
ADD PAGINATION 
 ****************************/
 function pagination_bar() {
    global $wp_query;
 
    $total_pages = $wp_query->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}
 
function helpwp_custom_logo_output( $html ) {
	$html = str_replace('custom-logo-link', 'navbar-brand pt-3', $html );
	return $html;
}
add_filter('get_custom_logo', 'helpwp_custom_logo_output', 10);