<?php /* Template Name: Contact Us */ ?>
<?php get_header(); ?>
<div class="inner-header-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-head">
                    <h2> <?php the_title(); ?> </h2>
                </div>
            </div>
        </div>
    </div>      
</div>
<div class="contact-form-wrapper main-wrap-blogs py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="contact-heading">
                    <h2 class="mb-4"> <?php the_title(); ?> </h2>
                </div>
                <?php echo do_shortcode( '[contact-form-7 id="102" title="Contact form 1"]' ); ?>
            </div>
             <div class="col-lg-4">
                <div class="inner-sidebar">
                    <!-- Sidebar -->
                        <?php require_once get_template_directory() . '/inc/sidebar.php'; ?>
                    <!-- End Sidebar -->
                </div>
            </div>
        </div>
    </div>
</div>



<?php get_footer(); ?>