


<!-- Footer -->

    <footer>
        <div class="footer-wrap-yellow">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="footer-box">
                                <?php if ( is_active_sidebar( 'quicklink_widget' ) ) : ?>
                                    <?php dynamic_sidebar( 'quicklink_widget' ); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-box">
                                <?php if ( is_active_sidebar( 'category_widget' ) ) : ?>
                                    <?php dynamic_sidebar( 'category_widget' ); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-box">
                                <?php if ( is_active_sidebar( 'contact_widget' ) ) : ?>
                                    <?php dynamic_sidebar( 'contact_widget' ); ?>
                                <?php endif; ?>
                                <!-- <h5>CONTACT</h5> -->
                                <!-- <ul class="default-list">
                                    <li>
                                        <a href="javascript:void(0)">
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 50 50" width="25" height="17" fill="#ffffff"><path d="M39.031 47h-.047c-7.515-.246-16.32-7.531-22.386-13.602-6.075-6.07-13.36-14.878-13.594-22.359-.086-2.625 6.355-7.293 6.422-7.34 1.672-1.164 3.527-.75 4.289.305.515.715 5.398 8.113 5.93 8.953.55.871.468 2.168-.22 3.469-.378.722-1.636 2.933-2.226 3.965.637.906 2.32 3.129 5.797 6.605 3.48 3.477 5.7 5.164 6.61 5.8 1.03-.589 3.242-1.847 3.964-2.226 1.282-.68 2.57-.765 3.45-.226.898.55 8.277 5.457 8.957 5.93.57.402.937 1.09 1.011 1.89.07.809-.18 1.664-.699 2.41-.043.063-4.656 6.426-7.258 6.426z"/></svg>
                                        </span> 0203-034-0579</a>
                                    </li>
                                    <li><a href="javascript:void(0)"> 
                                        <span>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 50 50" width="25" height="17" fill="#ffffff"><path d="M0 7v2.912c6.708 5.797 20.532 17.745 21.664 18.73 1.41 1.228 2.802 1.36 3.336 1.36.534 0 1.927-.131 3.336-1.357 1.09-.95 14.448-12.612 21.664-18.913V7H0zm50 5.383a63103.83 63103.83 0 01-20.354 17.765c-1.917 1.67-3.888 1.85-4.646 1.85s-2.73-.18-4.646-1.852C19.29 29.222 7.044 18.64 0 12.551V43h50V12.383z"/></svg>
                                        </span> info@domain.co.uk</a></li>
                                    <li>
                                        <div class="social-icons">
                                            <a href="javascript:void(0)">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 30 30" width="20" height="18" fill="#111519"><path d="M12 27V15H8v-4h4V8.852C12 4.785 13.981 3 17.361 3c1.619 0 2.475.12 2.88.175V7h-2.305C16.501 7 16 7.757 16 9.291V11h4.205l-.571 4H16v12h-4z"/></svg>
                                            </a>
                                            <a href="javascript:void(0)" class="ml-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 50 50" width="20" height="15" fill="#111519"><path d="M50.063 10.438a20.57 20.57 0 01-5.91 1.62 10.309 10.309 0 004.523-5.687 20.648 20.648 0 01-6.531 2.492 10.258 10.258 0 00-7.504-3.246c-5.68 0-10.286 4.602-10.286 10.281 0 .805.094 1.59.27 2.344-8.547-.43-16.121-4.523-21.195-10.746a10.243 10.243 0 00-1.39 5.172c0 3.566 1.812 6.715 4.573 8.562a10.274 10.274 0 01-4.66-1.289v.13c0 4.984 3.547 9.136 8.246 10.085a10.29 10.29 0 01-4.644.172c1.312 4.082 5.11 7.063 9.605 7.145A20.613 20.613 0 012.39 41.87c-.831 0-1.648-.047-2.449-.144a29.053 29.053 0 0015.762 4.62c18.914 0 29.258-15.667 29.258-29.253 0-.446-.012-.895-.027-1.332a20.904 20.904 0 005.129-5.325z"/></svg>
                                            </a>
                                        </div>
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyrights">
                                <p>© <?php echo date('Y') ?> Wisdom Bob All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </footer>
<!-- Footer -->


        <?php wp_footer(); ?>
    </body>
</html>