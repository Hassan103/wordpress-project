<?php get_header(); ?>


<!-- Blog Section -->

<div class="main-wrap-blogs">
    <div class="features-blogs">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading text-center mb-5">
                        <h1 class="sec-head">Features Blogs</h1>
                    </div>
                </div>
            </div>
            <div class="feature-blogs-list">
                <?php 
                    // query
                    $data = [
                        'post_type'			=> 'post',
                        'posts_per_page'	=> -1,
                        'meta_key'			=> 'is_feature',
                        'orderby'			=> 'meta_value',
                        'order'				=> 'DESC'
                    ];
                    $features_blogs = new WP_Query($data);
                ?>  
                <div class="ros"> 
                    <div class="feature-blog">  
                            <?php if( $features_blogs->have_posts() ) :  
                                while( $features_blogs->have_posts() ) : $features_blogs->the_post() ?>
                                    <div class="item">
                                        <div class="feature-blog-box">
                                            <figure>
                                                <?php
                                                    if( has_post_thumbnail() ) : 
                                                        the_post_thumbnail();
                                                    else : ?>
                                                    <img src="<?= get_template_directory_uri() . '/images/placeholder_featured_image.svg' ?>" alt="">
                                                <?php endif; ?>
                                                <figcaption>
                                                    <span class="cat"> <?php the_category(' '); ?> </span>
                                                    <h4> <?php echo wp_trim_words( get_the_title(), 10, '...' ); ?>  </h4>
                                                    <p><?php echo wp_trim_words( get_the_content(), 21, '...' );  ?></p>
                                                    <div class="feature-blog-links d-flex align-items-center justify-content-between w-100">
                                                        <span class="date">
                                                            <i class="iocn"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#cccccc" width="15" height="15"><path d="M12 0C5.371 0 0 5.371 0 12s5.371 12 12 12 12-5.371 12-12S18.629 0 12 0zm0 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2zm-1.063 1.875l-.437 8.188.094.874 6.156 5.438.969-1-5.094-5.406-.438-8.094z"></path></svg></i>
                                                            <?= the_time('F j, Y');?>
                                                        </span>
                                                        <div class="bp-btn">
                                                            <a href="<?php the_permalink(); ?>" class="m-0">Read More <span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 490.4 490.4" width="25" height="25">
                                                                <path d="M245.2 490.4c135.2 0 245.2-110 245.2-245.2S380.4 0 245.2 0 0 110 0 245.2s110 245.2 245.2 245.2zm0-465.9c121.7 0 220.7 99 220.7 220.7s-99 220.7-220.7 220.7-220.7-99-220.7-220.7 99-220.7 220.7-220.7z" class="path1"/>
                                                                <path class="path2" d="M138.7 257.5h183.4l-48 48c-4.8 4.8-4.8 12.5 0 17.3 2.4 2.4 5.5 3.6 8.7 3.6s6.3-1.2 8.7-3.6l68.9-68.9c4.8-4.8 4.8-12.5 0-17.3l-68.9-68.9c-4.8-4.8-12.5-4.8-17.3 0s-4.8 12.5 0 17.3l48 48H138.7c-6.8 0-12.3 5.5-12.3 12.3 0 6.8 5.5 12.2 12.3 12.2z"/></svg></span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                    </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query();	?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-widget-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12"><h4>Latest Articles</h4></div>
            </div>
            <div class="row">
                <!-- Main Blog Area -->
                    <div class="col-lg-8">
                        <div class="blog-section-listing">
                            <!-- POST LOOP -->
                            <?php 
                                // query_posts('posts_per_page=3'); 
                                if(have_posts()) :
                                    while(have_posts()) : the_post() ?>
                                        <!-- Blog Post -->
                                        <div class="blogpost-box">
                                            <div class="bp-img">                                                
                                                <figure>
                                                    <?php  
                                                    if ( has_post_thumbnail() ) :  
                                                        the_post_thumbnail();
                                                         else : ?>
                                                    <img src="<?php echo get_template_directory_uri() . '/images/placeholder_featured_image.svg' ?>" alt="" class="img-fluid">    
                                                    <?php endif; ?>
                                                </figure>
                                            </div>
                                            <div class="bp-details">
                                                <span class="catergory"><?=  the_category(' '); ?></span>
                                                <h2><?php echo wp_trim_words( get_the_title(), 10, '...' ); ?></h2>
                                                <span class="bp-date-time"> 
                                                    <i>
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#ffc15e" width="18" height="18" ><path d="M12 0C5.371 0 0 5.371 0 12s5.371 12 12 12 12-5.371 12-12S18.629 0 12 0zm0 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2zm-1.063 1.875l-.437 8.188.094.874 6.156 5.438.969-1-5.094-5.406-.438-8.094z"/></svg>
                                                    </i><?= the_time('F j, Y'); ?></span>
                                                <p><?php echo wp_trim_words( get_the_content(), 25, '...' );  ?></p>
                                                <div class="bp-btn">
                                                    <a href="<?php the_permalink(); ?>">Read More <span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 490.4 490.4" width="26" height="26">
                                                    <path d="M245.2 490.4c135.2 0 245.2-110 245.2-245.2S380.4 0 245.2 0 0 110 0 245.2s110 245.2 245.2 245.2zm0-465.9c121.7 0 220.7 99 220.7 220.7s-99 220.7-220.7 220.7-220.7-99-220.7-220.7 99-220.7 220.7-220.7z" class="path1"/>
                                                    <path class="path2" d="M138.7 257.5h183.4l-48 48c-4.8 4.8-4.8 12.5 0 17.3 2.4 2.4 5.5 3.6 8.7 3.6s6.3-1.2 8.7-3.6l68.9-68.9c4.8-4.8 4.8-12.5 0-17.3l-68.9-68.9c-4.8-4.8-12.5-4.8-17.3 0s-4.8 12.5 0 17.3l48 48H138.7c-6.8 0-12.3 5.5-12.3 12.3 0 6.8 5.5 12.2 12.3 12.2z"/></svg></span></a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Blog Post --> 
                                    <?php endwhile; 
                                endif;
                             ?>
                            <nav class="pagination"><?php pagination_bar(); ?></nav>
                             <!-- POST LOOP -->
                            <!-- Blog Post -->   
                    
                        </div>
                    </div>
                <!-- Main Blog Area -->
                <!-- Sidebar -->
                <div class="col-lg-4">
                    <?php require_once get_template_directory() . '/inc/sidebar.php'; ?>
                </div>
                <!-- End Sidebar -->
            </div>
        </div>    
    </div>
</div>
<!-- Blog Section -->



<?php get_footer(); ?>